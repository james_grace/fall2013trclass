/*
* Name: James Grace
* Student ID: 2162001
* Date: 8 October 2013
* Lab: 7
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

double power(double base, int expo)
{
       double final = pow(base, expo);
       return final;
}
double power(double base, double expo)
{
       double final = pow(base, expo);
       return final;
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    
    cout << "This program computes exponents of two given numbers.\n";
    cout << "Base: 2" << endl;
    cout << "Exponent: 3" << endl;

    cout << "2 to the power of 3 is " << power(2,3) << endl;
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
