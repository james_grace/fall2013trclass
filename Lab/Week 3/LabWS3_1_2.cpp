/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.1
* Problem: 2
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int num1, num2, swap;
    string dash = "-";
    
    cout << "Please enter two numbers less than 1000: \n";
    cin >> num1 >> num2;
    cout << "\n";
    
    if ( (num1<1000) && (num2<1000))
    {
    cout << "X = " << left << setw(3) << num1 << " Y = " << left << setw(3) << num2 << endl;
    cout << setfill('-') << left << setw(76) << dash << endl;
    swap = num1;
    num1 = num2;
    cout << setfill(' ') << "X = " << setw(3) << num1 << " Y = " 
    << setw(3) << swap << endl;
    cout << "\n";
    cout << "Thank you.\n";
    }
    else
    {
        cout << "One or both of the numbers given is incorrect.\n";
        cout << "Please close the program and try again. Thank you.\n";
        }
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
