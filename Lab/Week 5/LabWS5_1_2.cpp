/*
* Name: James Grace
* Student ID: 2162001
* Date: 1 October 2013
* Lab: 5.1
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
    bool run = true;
while (run)
{
    int take, total = 23, comp = 0;
    bool exe = true;
    cout << "This is the game '23'. Players take turns, withdrawing either 1, 2,\n";
    cout << "or 3 toothpicks at a time. The player to withdraw the last \n";
    cout << "toothpick loses the game. USERS start first.\n";
    cout << "Player 1: ";
    cin >> take;
    total = total - take;
    while (exe)
    {
    if (total > 4)
    {    
         cout << "The total is now: " << total << endl;
         comp = 4 - take;
         total = total - comp;
         cout << comp << endl;
         cout << "The total is now: " << total << endl;
         cout << endl;
         cout << "Player 1: ";
         cin >> take;
         total = total - take;
         }
    else if (total <= 4 && total >=2)
    {
         cout << "The total is now: " << total << endl;
         comp = total - 1;
         cout << comp << endl;
         total = total - comp;
         cout << "The total is now " << total << ". USER loses.\n";
         exe = false;
         }
    else if (total - take == 0)
    {
         cout << "The total is now: 0.";
         cout << " USER loses.\n";
         }
    else
    {
         cout << "The total is now " << total << ". USER wins.\n";
         exe = false;
         }
         }     
         
         
    cout << endl;
    cout << "Would you like to play again? (Y/N) ";
    cin >> ans;
    if (ans == 'n' || ans == 'N')
       {
            run = false;
       }
    cout << "\n";
    
}
    return EXIT_SUCCESS;
}
