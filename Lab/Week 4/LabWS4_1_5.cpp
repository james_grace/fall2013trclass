/*
* Name: James Grace
* Student ID: 2162001
* Date: 19 September 2013
* Lab: 4
* Problem: 5
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    srand(time(NULL));
    int x = rand(), held;
    held = x % 101 + 1;
    
    int guess;
     
    char ans;
do
{
    cout << "Welcome to the Circus of Values!\n";
    cout << "Guess the number within ten tries. Feeling lucky?\n";
    cout << "Valid answers must be between 1 and 100. Good luck!\n";
    cout << "Guess: ";
    
    for (int i = 0; i < 10; i++)
    {
        cin >> guess;
        
        if (guess >= 1 && guess <= 100)
        {
           if (guess - held == 0)
           {
               cout << "Congradulations, you guessed correctly!\n\a";
               i = 10;
           }
           else if (guess < held)
           {
               cout << "Higher.\n";
               }
           else if (guess > held)
           {
                cout << "Lower.\n";
           }        
        }
        else
        {
            cout << "This answer is not within parameters. Please try again.\n";
            }                  
    }
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
