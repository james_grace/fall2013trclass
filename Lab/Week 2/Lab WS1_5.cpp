/* 
 * File:   WS2_5.cpp
 * Author: James Grace
 * Lab Worksheet 2_5
 * Created on September 3, 2013, 4:13 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * The value of dif will be 9.26, due to subtraction.
 * The value of intDif will be 925 because of the static_cast.
 * The value of intReal will be 926 because of the previous statement of double.
 */
int main(int argc, char** argv) {
  
    
  double x = 10.76;
  double y = 1.50;
  const int mult = 100;
  
  double dif = x - y;
  cout << dif << endl;
  
  int intDif = static_cast<int>(dif * mult);
  cout << intDif << endl;
  cout << dif * mult << endl;

    return 0;
}

