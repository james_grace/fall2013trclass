/* Homework 3
 * File:   homework3.cpp
 * Author: James Grace
 * Use two variables to store two words. Print both words within a line of
 * "=" in between
 * Created on August 29, 2013, 3:46 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main() 
{

    string word = "Hello";
    string word2 = "World";
    string lines = "===============";
    
    cout << word << endl;
    cout << lines << endl;
    cout << word2 << endl;
    
    return 0;
}