/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 5, 2013, 5:04 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    double meters, feet, inches;
    const double mile = 1609.344;
    
    cout << "Hello. Please enter a measurement in meters to be converted: \n";
    cin >> meters;
    
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    
    feet = 5280*(meters/mile);
    inches = feet*12;
   
    cout << "There are " << feet << " feet and " 
            << inches << " inches in " << meters << " meters.\n";   
    
    return 0;
}

