/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.2
* Problem: 5
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    string grade;
    
    
    cout << "This program calculates grades.\n";
    cout << "Please enter a grade between B- and A+: ";
    cin >> grade;
    cout << "\n";
    
    
    if (grade == "A+")
    {
              cout << "This grade is equivalent to 100+\n";
              }
    
    else if (grade == "A")
    {
         cout << "This grade is equivalent to 93-100\n";
         }
    
    else if (grade == "A-")
    {
         cout << "This grade is equivalent to 90-92.9\n";
         }
    
    else if (grade == "B+")
    {
         cout << "This grade is equivalent to 87-89.9\n";
         }
         
    else if (grade == "B")
    {
         cout << "This grade is equivalent to 83-86.9\n";
         }
 
    else if (grade == "B-")
    {
         cout << "This grade is equivalent to 80-82.9\n";
         }
      
      
    system("PAUSE");  
    return EXIT_SUCCESS;
}
