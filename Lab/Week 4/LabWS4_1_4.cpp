/*
* Name: James Grace
* Student ID: 2162001
* Date: 19 September 2013
* Lab: 4
* Problem: 4
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
do
{
    int year;
    
    cout << "This program tests if the entered year is a leap year.\n";
    cout << "What year would you like to test? ";
    cin >> year;
    cout << "\n";
      
    if ((year % 400 == 0) && (year % 100 == 0) || (year % 4 == 0 && year % 100 != 0))
    {
         cout << year << " is a leap year.\n";
         cout << "\n";
         }
    else
    {
        cout << year << " is not a leap year.\n";
        cout << "\n";
        }
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} 
while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
