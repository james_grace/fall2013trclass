/*
* Name: James Grace
* Student ID: 2162001
* Date: October 7 2013
* HW: 5
* Problem: 9
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

const double gallonsinaliter = 0.264179;
double conver (double num1, double num2)
       {
       return (num2 / (num1 * gallonsinaliter));
       }


int main(int argc, char *argv[])
{
    char ans;
do
{
    ifstream infile;
    infile.open ("data.dat");
    
    if (!infile)
    {
       cout << "File error. No file found.\n";
       }     
    else
    {
    double miles, miles2, liters, liters2, mpg, mpg2;
    
    
      infile >> liters >> miles >> liters2 >> miles2;

      
      mpg = conver(liters, miles);
      mpg2 = conver(liters2, miles2);
      
      
      cout.setf(ios::fixed);
      cout.setf(ios::showpoint);
      cout.precision(2);

      cout << "Car #1 \n";
      cout << "        Liters used: " << liters << endl;
      cout << "        Miles driven: " << miles << endl;
      cout << "        Miles per Gallon: " << conver(liters, miles) << endl;
      cout << "\n";
      cout << "Car #2 \n";
      cout << "        Liters used: " << liters2 << endl;
      cout << "        Miles driven: " << miles2 << endl;
      cout << "        Miles per Gallon: " << conver(liters2, miles2) << endl;
      cout << "\n";

      if (mpg > mpg2)
         {
           cout << "Car #1 has better gas milage.\n";
           cout << "\n";
         }
      else if (mpg2 > mpg)
           {
              cout << "Car #2 has better gas milage.\n";
              cout << "\n";
           }
      else
           {
              cout << "These cars have equal gas milage.\n";
              cout << "\n";
           }
    
    infile.close();
    }
    cout << "Would you like to compute again? (Y/N) ";
    cin >> ans;
    cout << "\n";

} while (ans == 'y' || ans == 'Y');



    return EXIT_SUCCESS;
    system("PAUSE");
}
