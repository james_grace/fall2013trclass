/*
* Name: James Grace
* Student ID: 2162001
* Date: 22 October 2013
* Lab: 8
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cctype>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
    char store = ' ';
    ofstream outfile;
    outfile.open("data2.dat", ios::app);
    cout << "Enter whatever you'd like. When you're done, enter @.\n";
    do{
    store = getchar();
    outfile << store;
    }while(store != '@');
    outfile.close();
    
    return 0;
    return EXIT_SUCCESS;
}
