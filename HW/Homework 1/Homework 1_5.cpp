//James Grace
//Assignment 1_5
//4 September 2013
//This program adds and multiplies numbers and presents the sum and product.

#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
      //Define Variables
      /*These are based off of a Dungeons and Dragon's joke.*/
      int bards, eligible_women, offspring, broken_hearts;

      //Input
      cout << "Hello\n";
      cout << "Press return after entering number.\n";
      cout << "Enter the number of bards in party:\n";
      cin >> bards;
      cout << "Enter the number of eligible women in town:\n";
      cin >> eligible_women;

      //Processing
      offspring = bards * eligible_women;
      broken_hearts = bards + eligible_women;

      //Output
      cout << "If you have ";
      cout << bards;
      cout << " bards in the party\n";
      cout << "and ";
      cout << eligible_women;
      cout << " eligible women in town, then\n";
      cout << "you have ";
      cout << offspring;
      cout << " offspring when they leave town.\n";
      cout << "You may also have " << broken_hearts << " broken hearts.\n";
      cout << "Goodbye\n";



      system("PAUSE");
      return 0;
}
