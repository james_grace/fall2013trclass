/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.1
* Problem: 3
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int x = 5;
    int y = 0;

    if (x == 5)
    {
     y = 5;
    }
    else if (x == 8)
    {
      y = 6;
    }
    else
    {
       y = 7; 
    }

    cout << "X = " << x << " Y = " << y << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
