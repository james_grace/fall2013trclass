/*
* Name: James Grace
* Student ID: 2162001
* Date: 8 October 2013
* Lab: 7
* Problem: 6
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
do
{
    double n1 = 10, n2 = 12;
    cout << "Before swap: \n" << n1 << endl << n2 << endl;
    swap (n1, n2);
    cout << "After swap: \n" << n1 << endl << n2 << endl;
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}

void swap (double &x, double &y)
{
     int temp = x;
     x = y;
     y = temp;
}
