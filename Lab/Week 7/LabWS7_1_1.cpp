/*
* Name: James Grace
* Student ID: 2162001
* Date: 8 October 2013
* Lab: 7
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

double power(double base, int expo)
{
       double final = pow(base, expo);
       return final;
}

int main(int argc, char *argv[])
{
    char ans;
do
{
    double x;
    int y;
    
    cout << "This program computes exponents of two given numbers.\n";
    cout << "Base: ";
    cin >> x;
    cout << "Exponent: ";
    cin >> y;
    cout << endl;
    cout << x << " to the power of " << y << " is " << power(x,y) << endl;
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
