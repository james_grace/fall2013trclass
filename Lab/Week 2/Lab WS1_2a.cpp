/* 
 * File:   WS2_2a.cpp
 * Author: James Grace
 * Lab Worksheet 2_2b
 * Created on September 3, 2013, 4:41 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * The data from a is lost to the data from b.
 */
int main(int argc, char** argv) {

int a = 5;
int b = 10;
int c;
cout << "a: " << a << " " << "b: " << b << endl;
c = a;
a = b;
b = a;
cout << "a: "  << a << " " << "b: " << c << endl;    
    
    return 0;
}

