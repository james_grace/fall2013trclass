/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.1
* Problem: 1
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int singles, doubles, triples, homers, atbat;
    double slug_per;
    
    cout << "Hello!\n";
    cout << "This program compares baseball player's batting averages.\n";
    cout << "Please enter the player's singles: \n";
    cin >> singles;
    cout << "Please enter the player's doubles: \n";
    cin >> doubles;
    cout << "Please enter the player's triples: \n";
    cin >> triples;
    cout << "Please enter the player's home runs: \n";
    cin >> homers;
    cout << "Please enter the times the player is at bat: \n";
    cin >> atbat;
    cout << "\n";
    
    slug_per = (singles+(static_cast<double>(2)*doubles)+
    (static_cast<double>(3)*triples)+(static_cast<double>(4)*homers))/atbat;
    
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    
    cout << "The player's Slugging Percentage is: "
    << slug_per << "%" << endl;
    
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
