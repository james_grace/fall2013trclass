/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.2
* Problem: 4
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
      
    string x, combined;

    cout << "Please enter ten numbers. Press return after each input."; 
    cout << "\n";
    
    for(int i = 0; i < 10; i++)
    {
      cin >> x;
      combined += x + " ";     
      
    };
    
    cout << combined;
    cout << "\n";
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
