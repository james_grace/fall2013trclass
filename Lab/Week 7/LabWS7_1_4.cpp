/*
* Name: James Grace
* Student ID: 2162001
* Date: 8 October 2013
* Lab: 7
* Problem: 4
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

bool leap (int year)
{
    if ((year % 400 == 0) && (year % 100 == 0) || 
              (year % 4 == 0 && year % 100 != 0))
    {
         return true;
         }
    else
    {
        return false;
        }
}

int main(int argc, char *argv[])
{

do
{
    int date;
    
    cout << "This program tests if an entered year is a leap year.\n";
    cout << "Enter any letter to exit.\n";
    cout << "What year (or years) would you like to test? ";
    while (cin >> date)
    {
    cout << "\n";
    if (leap(date))
    {
       cout << date << " is a leap year.\n";
       cout << "\n";
       }
    else
    {
        cout << date << " is not a leap year.\n";
        cout << "\n";
        }
    }
    
}while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
