/* 
 * File:   Lab WS2_3.cpp
 * Author: James Grace
 * Lab Worksheet 2 Question 3
 * Created on September 5, 2013, 4:46 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int test1=0, test2=0, test3=0, avg=0;
    
    cout << "This program will average three test scores.\n";
    cout << "These test scores are to be out of 100 points.\n";
    cout << "Enter first test score: ";
    cin >> test1;
    cout << "Enter second test score: ";
    cin >> test2;
    cout << "Enter third test score: ";
    cin >> test3;
    
    avg = (test1 + test2 + test3) / 3;
    
    cout << "Your average is: " << avg << endl;
    cout << "Goodbye.\n";                                                          
    
    return 0;
}

