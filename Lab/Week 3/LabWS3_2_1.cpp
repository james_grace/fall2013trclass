/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.2
* Problem: 1
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    
    double purchase, payment, change;
    int total, dollars, quarters, dimes, nickles, pennies;
    
    char ans;
do
{
    cout << "Enter the total cost of the item: $";
    cin >> purchase;
    cout << "\n";
    cout << "Enter the total amount of payment being made: $";
    cin >> payment;
    
    while (payment < purchase)
    {
          cout << "You do not have enough to purchase this item.\n\a";
          cout << "Please try again.\n";
          cin >> payment;
          }
    
    change = (payment - purchase ) * 100;
    total = static_cast<int>(change);
    
    dollars = total / 100;
    quarters = (total % 100) / 25;
    dimes = ((total % 100) - (quarters * 25))  / 10;
    nickles = ((total % 100) - (quarters * 25) - (dimes * 10)) / 5;
    pennies = total % 10;
    
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << "Your return change is: $" << change/100 << endl;
    cout << "Your exact change is " << dollars << " dollars\n";
    cout << quarters << " quarters\n";
    cout << dimes << " dimes\n";
    cout << nickles << " nickles\n";
    cout << "and " << pennies << " pennies.\n";
    cout << "\n";        
            
    cout << "Do you wish to run again?\n";
    cout << "Enter y for yes, n for no: \n";
    cin >> ans;
    cout << "\n";
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    return EXIT_SUCCESS;
}
