/*
* Name: James Grace
* Student ID: 2162001
* Date: 3 October 2013
* Lab: 5.1
* Problem: 4
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <cctype>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
    cout << "This program determines if an entered word is a palindrome.\n";
do
{
    string word;
    string reverse = "";
    
    cout << "Enter a word: ";    
    cin >> word;
    
    string before = word;
    int size = word.size();
    for (int z = 0; z < size; z++)
    {
        char upper = word[z];
        upper = toupper(upper);
        word[z] = upper;
    }
    
    for (int i = size; i > 0; i--)
    {
        reverse += word[i - 1];
        }
      if (word == reverse)
       {
         cout << before << " is a palindrome.\n";
         cout << endl;
       }
      else
          {
             cout << before << " is not a palindrome.\n";
             cout << endl;
          }

    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
