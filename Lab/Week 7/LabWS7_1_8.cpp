/*
* Name: James Grace
* Student ID: 2162001
* Date: 15 October 2013
* Lab: 7
* Problem: 8
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;


int main(int argc, char *argv[])
{
    char ans;
do
{
    
    
    cout << endl << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
