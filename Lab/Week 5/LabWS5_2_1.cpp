/*
* Name: James Grace
* Student ID: 2162001
* Date: 26 September 2013
* Lab: 5.2
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{

// I have no idea what this program is computing.
         int n;
         cin >> n;
         double x = 0;
         double s;
         bool run = true;
         while (run)
         {
         	s = 1.0 / (1 + n * n);
	        n++;
         	x = x + s;
        	if (s < 0.01)
        	{
                run = false;
                }  
        	
          }
          
          cout << x << endl;
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
