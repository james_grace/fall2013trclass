/*
* Name: James Grace
* Student ID: 2162001
* Date:  2013
* Lab: 5.1
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

double max(double num1, double num2)
{
       double high = 0;
       if (num1 > num2)
          high = num1;
       else
           high = num2;
       return high;
}

int main(int argc, char *argv[])
{
    char ans;
do
{
    double balance, totamtdue, intpay;
    
    cout << "This program computes the interest due, total amount due, \n" 
         "and the minimum payment for a revolving credit account.\n";
    cout << "Please input the account balance: ";
    cin >> balance;
    
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    
    if (balance <= 1000.0)
    {
       intpay = balance * .015;
       totamtdue = balance + intpay;
       if (totamtdue <= 10.0)
       {
            cout << "Interest Due: " << intpay << endl;
            cout << "Total Amount Due: " << totamtdue << endl;
            cout << "Minimum Payment: " << totamtdue << endl;
            cout << endl;
           }
       else
       {
            double totamtint = totamtdue * .01;
            cout << "Interest Due: " << intpay << endl;
            cout << "Total Amount Due: " << totamtdue << endl;
            cout << "Minimum Payment: " << max(10.0, totamtint) << endl;
            cout << endl;
           }
       }
    else
    {
        intpay = balance * .01;
        totamtdue = balance + intpay;
        if (totamtdue <= 10.0)
        {
            cout << "Interest Due: " << intpay << endl;
            cout << "Total Amount Due: " << totamtdue << endl;
            cout << "Minimum Payment: " << totamtdue << endl;
            cout << endl;
           }
        else
        {
            double totamtint = totamtdue * .01;
            cout << "Interest Due: " << intpay << endl;
            cout << "Total Amount Due: " << totamtdue << endl;
            cout << "Minimum Payment: " << max(10.0, totamtint) << endl;
            cout << endl;
           }
        }
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
