/*
* Name: James Grace
* Student ID: 2162001
* Date: 15 October 2013
* Lab: 7
* Problem: 7
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <cctype>
#include <string>

using namespace std;

bool checkpal(string check)
{
       string reverse = "";
       string before = check;
       int size = check.size();
       for (int z = 0; z < size; z++)
       {
           char upper = check[z];
           upper = toupper(upper);
           check[z] = upper;
           }
    
       for (int i = size; i > 0; i--)
       {
           reverse += check[i - 1];
           }
       if (check == reverse)
       {
          return true;
          }
       else
       {
           return false;
           }
       
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    string word;
    cout << "This program recognizes palindromes. Enter a word: ";
    cin >> word;
    
    if (checkpal(word))
    {
       cout << word << " is a palindrome.\n";
       }
    else
    {
        cout << word << " is not a palindrome.\n";
        }
    
    cout << endl << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
