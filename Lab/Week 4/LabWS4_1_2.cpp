/*
* Name: James Grace
* Student ID: 2162001
* Date: 19 September 2013
* Lab: 4
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{


    int n;
    double guess, fguess, r, check, lguess;
    
    cout << "The program finds the square root of an entered number.\n";
    cout << "Please enter a number: ";
    cin >> n;
    cout << "\n";
    
    guess = n / 2;
    
    do
    {
          r = n / guess;
          lguess = guess;
          guess = (guess + r) / 2.0;
          check = lguess - guess;
          
    }while ( check > lguess * .01);
    
   
    cout << "The square root of " << n << " is " << guess << endl;
    
    

    
    system("PAUSE");
    return EXIT_SUCCESS;
}
