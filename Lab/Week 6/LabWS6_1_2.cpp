/*
* Name: James Grace
* Student ID: 2162001
* Date: 1 October 2013
* Lab: 6.1
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;
//Function
double sqre(double num) //Header
{
    return num * num;
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    double x;
    cout << "Enter a number to find the square: ";
    cin >> x;
    cout << endl;
    cout << "The answer is: " << sqre(x) << endl;
    cout << endl;
    
    
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');

    
    
    return EXIT_SUCCESS;
}
