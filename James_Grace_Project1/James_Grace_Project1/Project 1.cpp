/*
* Name: James Grace
* Student ID: 2162001
* Date: October 19 2013
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

//Functions

/*
* Totalearn helps calculate the total accrued wins or losses of prize money.
* Parameters: Two values (integers) representing the previous total and the amount bet.
* Return: The total loss or earnings of the placed bet.
* Problem I'm running into is that it will not save the previous totals and add that all up.
* Each time I complete a game it will restart at having 100 funds rather than the new amount.
* All testing has been done with bets of 10, but other bets have a similar result.
*/
int totalearn (int wallet, int bets, int &x)
{
          if (x == 0)
          {
          wallet = 100;
          if (bets >= 0)
          {
               int earnings;
               earnings = wallet + (bets * 2);
               return earnings;
               }
          else if (bets < 0)
          {
               int loss;
               bets = (bets * -1);
               loss = wallet - bets;
               return loss;
               }
           }
        else if (x >= 1)
        {
            if (bets >= 0)
          {
               int earnings;
               earnings = wallet + (bets * 2);
               return earnings;
               }
          else if (bets < 0)
          {
               int loss;
               bets = (bets * -1);
               loss = wallet - bets;
               return loss;
               }
          }
          x++;
}


//Function to "pull" card
int card()
{     
     int card = 0;
     card = rand() % 11 + 2;
     if (card == 11)
     {
          card = 10;
          return card;
          }
     else if (card == 12)
     {
          card = 10;
          return card;
          }
     else if (card == 13)
     {
          card = 10;
          return card;
          }
     else if (card == 14)
     {
          card = 10;
          return card;
          }
     else
         return card;     
}

//Card list for blackjack
string deck (int cardp)
{
string result;
ostringstream convert;
convert << cardp;
result = convert.str();
     if (result == "2")
     {
          return "2";
              }
     else if (result == "3")
     {
          return "3";
          }
     else if (result == "4")
     {
          return "4";
          }              
     else if (result == "5")
     {
          return "5";
          }
     else if (result == "6")
     {
          return "6";
          }
     else if (result == "7")
     {
          return "7";
          }
     else if (result == "8")
     {
          return "8";
          }
     else if (result == "9")
     {
          return "9";
          }
     else if (result == "10")
     {
          int ranface = rand() % 5 + 1;
          if(ranface == 1)
          {
             return "10";
             }
          else if (ranface == 2)
          {
               return "J";
               }
          else if (ranface == 3)
          {
               return "Q";
               }
          else if (ranface == 4)
          {
               return "K";
               }
     }
     else if (result == "11")
     {
          return "A";
          }     
}


/*Bool to help with continuous dice rolling loop.
* Parameters: Two values (integers): first is the second diceroll, second is the new goal to
*             be rolled.
* Return: False whether the goal was achieved or failed, true for when the chosen values
*         were not achieved (to continue rolling).
*/
bool rolling (int diceroll, int goal)
{
     if (diceroll == 7)
     {
        return false;
        }
     else if (diceroll == goal)
     {
          return false;
          }
     else
         return true;
}

//Bool for player decision in blackjack
bool loop(char ans)
{
     if (ans == 'Y' || ans == 'y')
        return true;
     else
         return false;
}

//Bool for telling dealer to hit or not
bool dealercalc (int hand)
{
     if (hand < 16)
     {
        return true;
        }
     else if (hand >= 16 && hand <= 21)
     {
        return false;
          }
     else
        return false;
}

//Declaring first use of void commands
void craps();
void blackjack();

int main(int argc, char *argv[])
{
    //Setting the variables
    srand(time(NULL));
    char game, ans, ans2;
    int x = 0;
    int bet = 0, roll, total;
    

    cout << "  CCCCCCCC        AA         SSS     IIIIIIIIII  NNN       NN    OOOOOOOO \n";                           
    cout << " CC      CC     AA  AA     SS   SS       II      NNN       NN   OO      OO\n";        
    cout << "CC        CC   AA    AA   SS     SS      II      NN N      NN  OO        OO\n";  
    cout << "CC            AA      AA  SS             II      NN  N     NN  OO        OO\n";  
    cout << "CC            AA      AA  SS             II      NN   N    NN  OO        OO\n"; 
    cout << "CC            AA      AA   SSSSSSS       II      NN    N   NN  OO        OO\n";      
    cout << "CC            AAAAAAAAAA         SS      II      NN     N  NN  OO        OO\n";
    cout << "CC            AA      AA         SS      II      NN      N NN  OO        OO\n";
    cout << "CC        CC  AA      AA  SS     SS      II      NN       NNN  OO        OO\n"; 
    cout << " CC      CC   AA      AA   SS   SS       II      NN       NNN   OO      OO\n";                  
    cout << "  CCCCCCCC    AA      AA     SSS     IIIIIIIIII  NN       NNN    OOOOOOOO\n"; 
    cout << endl;                          
    cout << "Welcome to the Casino! You begin with $100 worth of chips.\n";
    cout << "Please place your bet in a whole number amount.\n";
do
{
    cout << "Would you like to play: (A) Craps, (B) Blackjack or (C) Slots?\n";
    cin >> game;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    
    //Game option menu
    switch (game)
    {
           //Game 1: Craps
           case 'A':
                cout << "  CCCCCCCC    RRRRRRRRR          AA      PPPPPPPP       SSS   \n";                           
                cout << " CC      CC   RR      RR       AA  AA    PP      PP   SS   SS \n";        
                cout << "CC        CC  RR       RR     AA    AA   PP      PP  SS     SS \n";  
                cout << "CC            RR       RR    AA      AA  PP      PP  SS         \n";  
                cout << "CC            RR      RR     AA      AA  PP      PP  SS         \n"; 
                cout << "CC            RRRRRRRRRR     AA      AA  PPPPPPPP     SSSSSSS   \n";      
                cout << "CC            RR      RR     AAAAAAAAAA  PP                 SS  \n";
                cout << "CC            RR       RR    AA      AA  PP                 SS  \n";
                cout << "CC        CC  RR       RR    AA      AA  PP          SS     SS  \n"; 
                cout << " CC      CC   RR       RR    AA      AA  PP           SS   SS   \n";                  
                cout << "  CCCCCCCC    RR       RR    AA      AA  PP             SSS     \n";
                cout << endl;
                cout << "You've chosen to play Craps. After placing your bet, dice are\n"
                "rolled. If the initial dice total is a 7 or 11, you win! If the dice come\n"
                "up as a total of 2, 3, or 12 then you lose. All other rolls are now your\n"
                "targeted total. You keep rolling until you have reached either your old\n"
                "total, in which case you win, or the total comes up a 7, in which case\n"
                "you lose.\n";
                craps ();
                break;
                
           //Game 2: Blackjack
           case 'B':
    cout << "BBBBBBB  LL        AAA      CCCCC KK   KK      JJJJJJJ AAA      CCCCC KK   KK\n";                           
    cout << "BB     B LL       A   A    CC     KK  KK          JJ  A   A    CC     KK  KK\n";        
    cout << "BB     B LL      A     A  CC      KK KK           JJ A     A  CC      KK KK \n";  
    cout << "BB     B LL      A     A CC       KK K            JJ A     A CC       KK K \n";  
    cout << "BB     B LL      A     A CC       KKK             JJ A     A CC       KKK \n"; 
    cout << "BBBBBBB  LL      AAAAAAA CC       KKK             JJ AAAAAAA CC       KKK \n";      
    cout << "BB     B LL      A     A CC       KK K            JJ A     A CC       KK K \n";
    cout << "BB     B LL      A     A CC       KK KK           JJ A     A CC       KK  KK\n";
    cout << "BB     B LL      A     A  CC      KK  KK   JJ     JJ A     A  CC      KK   KK\n";
    cout << "BB     B LL      A     A   CC     KK   KK   JJ    JJ A     A   CC     KK    KK\n";                
    cout << "BBBBBBB  LLLLLLL A     A    CCCCC KK    KK   JJJJJJ  A     A    CCCCC KK     KK\n";
    cout << endl;     
                 cout << "You've chosen to play Blackjack. After placing your bet, both the\n"
                 "player and the dealer are dealt two cards: one face down and the other face\n"
                 " up. The player may view their second card any time they wish. Each card\n"
                 "has a value, with number cards having their face value while the Jack,\n"
                 "Queen, and King are worth 10. The Ace is worth either a total of 1\n"
                 "(known as hard) or 11 (known as soft). The point of the game is to get as\n"
                 "close to a total of 21 without going over, as well as having a higher total\n"
                 "than the dealer. If you go over or have a total smaller than the dealer,\n"
                 "you lose.\n";
                 blackjack();                        
                 break;
                 
           //Game 3: Slots      
           case 'C':
                cout << "This is where you could have played the slots. But they were\n"
                        "eaten by a grue. Too bad.\n\n\n\n\nYou were eaten by a grue.\n";
                
                
                break;
           default:
                cout << "The given answer does not comply. Please try "
                "capitalizing your letter.\n";
                cin >> game;
                cout << endl;   
                   
    }
    cout << "Play a different game?(Enter: Y/y for yes) ";
    cin >> ans;
    cout << endl;     
}
while (ans == 'y' || ans == 'Y');

    return EXIT_SUCCESS;
}

void craps ()
{
int die1, die2, sum, sum2, bet, total, x = 0;
char ans;
                
                do
                {
                cout << "Bet Amount: $";
                cin >> bet;
                cout << endl;
                
                //Rolls the first two dice
                die1 = rand() % 6 + 1;
                die2 = rand() % 6 + 1;
                sum = die1 + die2;
                
                cout << "Die rolled are " << die1 << " and " << die2 << " for a total of "
                     << sum << ".\n";
                     
                //Checks the sum to determine the result
                     if (sum == 7 || sum == 11)
                     {
                         cout << "You win!\n";
                         cout << "You now have $" << totalearn(total, bet, x) << endl;
                             }
                     else if ((sum == 2 || sum == 3) || sum == 12)
                     {
                         cout << "You lose.\n";
                         bet = bet * -1;
                         cout << "You now have $" << totalearn(total, bet, x) << endl;
                             }
                //If neither of the first two, then round 2 beings.
                     else
                     {
                         cout << "New total is " << sum << endl;
                         do
                         {
                /*Rolls two different dice to check against the first two, otherwise an
                infinite loop occurs
                */
                         int die3 = rand() % 6 + 1;
                         int die4 = rand() % 6 + 1;
                         sum2 = die3 + die4;
                         if (sum2 == 7)
                         {
                            cout << "You rolled " << die3 << " and " << die4 << " for a "
                                  "total of " << sum2 << endl;
                            cout << "You lose.\n";
                            bet = bet * -1;
                            cout << "You now have $" << totalearn(total, bet, x) << endl;
                            
                            } 
                         else if (sum2 == sum)
                         {
                              cout << "You rolled " << die3 << " and " << die4 << " for a "
                                  "total of " << sum2 << endl;
                              cout << "You win!\n";
                              cout << "You now have $" << totalearn(total, bet, x) << endl;
                              
                                   }
                         else
                         {
                             cout << "You rolled " << die3 << " and " << die4 << " for a "
                                  "total of " << sum2 << endl;
                             }
                           }while (rolling(sum2, sum) == true);
                     }
                     total += totalearn (total, bet, x);
                     
                cout << endl;
                cout << "Play again?(Enter: Y/y for yes) ";
                cin >> ans;
                cout << endl;
                } while (ans == 'y' || ans == 'Y');
}

void blackjack()
{
     //Variables listed and initialized
     int bet, dtotal = 0, total = 0, card1 = 0, card2 = 0, dcard1 = 0, dcard2 = 0, 
              nextcard = 0, bust = 0, x = 0;
     char ans, hit;
     do
     {
      cout << "Bet Amount: $";
      cin >> bet;
      cout << endl;
      
      
      card1 = card();
      card2 = card();
      dcard1 = card();
      dcard2 = card();
      total = card1 + card2;
      
      cout << "Your first two cards are: \n";
      cout << "--------------------------\n";
      cout << "        " << deck(card1) << "        " << deck(card2) << endl;
      cout << "--------------------------\n";
      cout << "With your total being: " << total << endl;
      cout << "Dealer's face up card is: " << deck(dcard1) << endl;
      cout << "Do you wish to hit? (Y/y for yes) ";
           
      //Decision to get next card
      do
      {
             cin >> hit;
             if(loop(hit))
             {
             nextcard = 0;
             nextcard = card();
             cout << "Your next card is: " << deck(nextcard) << endl;
             total += nextcard;
             cout << "Your total is: " << total << endl;
             cout << "Do you wish to hit? (Y/y for yes) ";
             }
             else
             {
                 cout << "Your total is: " << total << endl;
                 loop(hit) == false;
                 }
             }while(loop(hit));            
      
      //Dealer's turn  
      cout << "Dealer's face down card is: " << deck(dcard2) << endl;
      dtotal = dcard1 + dcard2;
      //If first card is an Ace
      if (dcard1 == 11 && dtotal <= 17)
      {       
         dcard1 = 1;
         dtotal = dcard1 + dcard2;
              if (dtotal < 17)
              {            
                do
                {
                  if(dealercalc(dtotal))
                  {
                  int dnext = 0;
                  dnext = card();
                  cout << "Dealer hits. Dealer's next card is " << deck(dnext) << endl;
                  dtotal += dnext;
                  }
                  else
                  {
                      cout << "Dealer's total is: " << dtotal << endl;
                      loop(hit) == false;
                      }
                  }while(dealercalc(dtotal));
              }
              else if ((dtotal >= 17 && dtotal <= 21))
              {
                  cout << "Dealer stays. Dealer's total is " << dtotal << endl;
              }
                  
              else
              {
                  cout << "Dealer has busted! You win!\n";
                  cout << "You now have $" << totalearn(total, bet, x) << endl;         
                  }
      }
      //If second card is an Ace
      else if (dcard2 == 11 && dtotal <= 17)
      {
          dcard2 = 1;
          dtotal = dcard1 + dcard2;
              if (dtotal < 17)
              {            
                do
                {
                  if(dealercalc(dtotal))
                  {
                  int dnext = 0;
                  dnext = card();
                  cout << "Dealer hits. Dealer's next card is " << deck(dnext) << endl;
                  dtotal += dnext;
                  }
                  else
                  {
                      cout << "Dealer's total is: " << dtotal << endl;
                      loop(hit) == false;
                      }
                  }while(dealercalc(dtotal));
              }
              else if ((dtotal >= 17 && dtotal <= 21))
              {
                  cout << "Dealer stays. Dealer's total is " << dtotal << endl;
              }
                  
              else
              {
                  cout << "Dealer has busted! You win!\n";
                  cout << "You now have $" << totalearn(total, bet, x) << endl;          
                  }
          
          
          }
      //If both cards are Aces
      else if (dcard1 == 11 && dcard2 == 11)
      {
          dcard2 = 1;
          dtotal = dcard1 + dcard2;
              if (dtotal < 17)
              {            
                do
                {
                  if(dealercalc(dtotal))
                  {
                  int dnext = 0;
                  dnext = card();
                  cout << "Dealer hits. Dealer's next card is " << deck(dnext) << endl;
                  dtotal += dnext;
                  }
                  else
                  {
                      cout << "Dealer's total is: " << dtotal << endl;
                      loop(hit) == false;
                      }
                  }while(dealercalc(dtotal));
              }
              else if ((dtotal >= 17 && dtotal <= 21))
              {
                   cout << "Dealer stays. Dealer's total is " << dtotal << endl;
              }
              else
              {
                  cout << "Dealer has busted! You win!\n";
                  cout << "You now have $" << totalearn(total, bet, x) << endl;
                  } 
          
          
          }
      //Otherwise, if no Aces are in dealer's hand             
      else
          {
              if (dtotal < 17)
              {            
                do
                {
                  if(dealercalc(dtotal))
                  {
                  int dnext = 0;
                  dnext = card();
                  cout << "Dealer hits. Dealer's next card is " << deck(dnext) << endl;
                  dtotal += dnext;
                  }
                  else
                  {
                      cout << "Dealer's total is: " << dtotal << endl;
                      loop(hit) == false;
                      }
                  }while(dealercalc(dtotal));
              }
              else if ((dtotal >= 17 && dtotal <= 21))
              {
                   cout << "Dealer stays. Dealer's total is " << dtotal << endl;
              }
              else
              {
                cout << "Dealer has busted! You win!\n";
                cout << "You now have $" << totalearn(total, bet, x) << endl;
                }  
              }
      //Comparing totals
      if (total > dtotal && total < 22)
      {
           cout << "You win!\n";
           cout << "You now have $" << totalearn(total, bet, x) << endl;
                }
      else if (total > 22)
      {
           
           cout << "You busted! Sorry.\n";
           bet = bet * -1;
           cout << "You now have $" << totalearn(total, bet, x) << endl;
           }
      else if (dtotal > total && dtotal < 22)
      {
           cout << "Dealer's total is: " << dtotal << endl;
           cout << "Dealer wins.\n";
           bet = bet * -1;
           cout << "You now have $" << totalearn(total, bet, x) << endl;
           }
      else if (total == dtotal && ((total < 22) && dtotal < 22))
      {
          cout << "You have tied with the Dealer.\n";
          cout << "You get your bet back. You now have $" << totalearn(total, 0, x) << endl;
          }
     cout << "Play again?(Enter: Y/y for yes) ";
     cin >> ans;
     cout << endl;
     } while (ans == 'y' || ans == 'Y');
}







