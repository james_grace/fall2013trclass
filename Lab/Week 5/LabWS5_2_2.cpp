/*
* Name: James Grace
* Student ID: 2162001
* Date: 26 September 2013
* Lab: 5.2
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

double max(double num1, double num2)
{
       double high = 0;
       if (num1 > num2)
          high = num1;
       else
           high = num2;
       return high;
}


int main(int argc, char *argv[])
{
    double first, second, x = 2, y = 15, z = 22, a = 14, b = 5, c = 16, 
           d = 50, e = 44, f = 30 , g = 8;
    int store;
    
    cout << "This program compares numbers and keeps the greater value.\n";
    cout << "Enter two numbers to compare: ";
    cin >> first >> second;
    cout << "\n";
    
    store = max(first, second);
    store = max(store, x);
    store = max(store, y);
    store = max(store, z);
    store = max(store, a);
    store = max(store, b);
    store = max(store, c);
    store = max(store, d);
    store = max(store, e);
    store = max(store, f);
    store = max(store, g);
    
    cout << "Of your two numbers, " << max(first, second) << 
    " is the greater number.\n";
    cout << endl;
    cout << "Compared to the numbers 2, 15, 22, 14, 5, 16, 50, 44, 30 and 8\n"
          << " the number " << store << " is the largest.\n";
    
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
