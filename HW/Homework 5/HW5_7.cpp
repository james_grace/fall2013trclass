/*
* Name: James Grace
* Student ID: 2162001
* Date: October 7 2013
* HW: 5
* Problem: 7
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

const double gallonsinaliter = 0.264179;
double conver (double num1, double num2)
       {
       return (num2 / (num1 * gallonsinaliter));
       }


int main(int argc, char *argv[])
{
    char ans;
do
{
    ifstream infile;
    infile.open ("data.dat");
    
    if (!infile)
    {
       cout << "File error. No file found.\n";
       }     
    else
    {
    double mpg, miles, liters;
    
    infile >> liters;
    infile >> miles;
    
    
    cout << "Car Facts: \n";
    cout << "        Liters used: " << liters << endl;
    cout << "        Miles driven: " << miles << endl;
    cout << "        Miles per Gallon: " << conver(liters, miles) << endl;
    cout << "\n";
    
    infile.close();
    }
    cout << "Would you like to compute again? (Y/N) ";
    cin >> ans;
    cout << "\n";

} while (ans == 'y' || ans == 'Y');



    return EXIT_SUCCESS;
}
