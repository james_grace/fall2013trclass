/*
* Name: James Grace
* Student ID: 2162001
* Date: 18 September 2013
* HW: 3
* Problem: 5
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int num;
    int posSum = 0;
    int negSum = 0;
    int totalSum = 0;
    char ans;
do
{
    cout << "Please enter in ten whole numbers, either positive or negative.\n";
    for(int i = 0; i < 10; i++)
    {
      cin >> num;
      cout << "You entered: " << num << endl;
      if (num > 0)
         {
          posSum += num;
          }
       else
       {
           negSum += num;
        }
    
        totalSum += num;
      
      }
      
    
    cout << "Pos Sum: " << posSum << endl;
    cout << "Neg Sum: " << negSum << endl;
    cout << "Total Sum: " << totalSum << endl;
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
