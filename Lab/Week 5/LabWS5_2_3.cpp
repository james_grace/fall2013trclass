/*
* Name: James Grace
* Student ID: 2162001
* Date: 3 October 2013
* Lab: 5.2
* Problem: 3
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;
//Function
int max(int num1, int num2)
{
       int high = 0;
       if (num1 > num2)
          high = num1;
       else
           high = num2;
       return high;
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    int numA, numB;
    cout << "This program compares numbers and keeps the greater value.\n";
    cout << "Enter two numbers to compare: ";
    cin >> numA >> numB;
    cout << "\n";
    
    cout << "Of the two numbers, " << max(numA, numB) << 
    " is the greater number.\n";
    
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');

    
    
    return EXIT_SUCCESS;
}
