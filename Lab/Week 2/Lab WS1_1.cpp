/* 
 * File:   WS2_1.cpp
 * Author: James Grace
 * Lab Worksheet 2_1
 * Created on September 3, 2013, 4:41 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * HAL 9000 greets his next victim.
 */
int main(int argc, char** argv) {

    string name;
    
    cout << "Hello, my name is HAL!\n";
    cout << "What is your name?\n";
    
    cin >> name;
    
    cout << "Hello, " << name << ". I am glad to meet you.\n";
    
    
    
    return 0;
}

