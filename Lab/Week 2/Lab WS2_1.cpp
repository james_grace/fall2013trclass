/*
James Grace
LabWS 2_1
5 September 2013
*/
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int x;
    
    cout << "Hello. Please choose a number: \n";
    cin >> x;
    cout << "Your new number is: " << x + 5 << endl;
    cout << "Goodbye.\n";
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
