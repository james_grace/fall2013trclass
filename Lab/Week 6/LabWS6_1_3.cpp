/*
* Name: James Grace
* Student ID: 2162001
* Date: 1 October 2013
* Lab: 6.1
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int read(string num)
{
    return num.size();
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    string words;
    
    cout << "This program counts characters. "
    "Please enter something to count:\n";
    getline(cin, words);
    cout << "There are " << read(words) << " characters entered.\n";
    cout << endl;
    
    
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
