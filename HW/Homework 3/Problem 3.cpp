/*
* Name: James Grace
* Student ID: 2162001
* Date: 18 September 2013
* HW: 3
* Problem: 3
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

const double mton1 = 35273.92;

int main(int argc, char *argv[])
{
    
    char ans;
do
{
    double box, amtbox, mton; 
    cout << "This program calculates the amount of cereal needed to make one";
    cout << " metric ton.\n";
    cout << "Please enter the weight of the chosen cereal box in ounces: ";
    cin >> box;
    cout << "\n";
    
    mton = box / mton1;
    amtbox = (mton1 / box);
    
    cout << "This box of cereal weighs " << mton << 
         " metric tons and you will\n";
    cout << "need " << static_cast<int>(amtbox) << 
         " boxes to make one metric ton.\n";
    cout << "\n";
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
