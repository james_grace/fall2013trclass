/*
* Name: James Grace
* Student ID: 2162001
* Date: 1 October 2013
* Lab: 6.1
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int read(string num)
{
    return num.size();
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    ifstream infile;
    infile.open ("test.dat");
    
    if (!infile)
    {
       cout << "File error. No file found.\n";
       }     
    else
    {
    string words;
    
    getline(infile, words);
    cout << "There are " << read(words) << " characters entered in this file.\n";
    infile.close();
    }
    
    cout << endl;
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";

} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
