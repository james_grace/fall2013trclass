/*
* Name: James Grace
* Student ID: 2162001
* Date: 24 September 2013
* Lab: 5.1
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int n;
    char ans;
do
{
    
    cout << "Enter a number: ";
    cin >> n;
    cout << endl;
    
    for (int x = 0; x < n / 2; x++)
    {
         for (int y = 0; y < n; y++)
         {
            if (y / 2 == x && y % 2 == 1)
               {
                cout << "-";
                }            
            else 
                 {
                 cout << "*";
                 }
         }
        cout << endl;
    }
         


    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";  
    return EXIT_SUCCESS;
}
