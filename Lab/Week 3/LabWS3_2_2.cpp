/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 3.2
* Problem: 2
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int grade;
    
    
    cout << "This program calculates grades.\n";
    cout << "Please enter a grade between 1 and 100: ";
    cin >> grade;
    cout << "\n";
    
    while (grade < 0 || grade > 100)
    {
          cout << "This is an incorrect value.";
          cout << " Please enter a value within the parameters. ";
          cin >> grade;
          cout << "\n"; 
          }
    
    if (grade <= 59)
    {
              cout << "This grade is an F. Try better next time.\n";
              }
    
    else if (grade <= 69)
    {
         cout << "This grade is a D.\n";
         }
    
    else if (grade <= 79)
    {
         cout << "This grade is a C.\n";
         }
    
    else if (grade <= 89)
    {
         cout << "This grade is a B.\n";
         }
         
    else if (grade <= 100)
    {
         cout << "This grade is an A. Great job!\n";
         }
      
      
    system("PAUSE");  
    return EXIT_SUCCESS;
}
