/*
* Name: James Grace
* Student ID: 2162001
* Date:  2013
* Lab: 
* Problem: 
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;
bool compare(string x, string y)
{
     if (x.size() == y.size())
     {
          for (int i = 0; i <= x.size(); i++)
          {
              if (x[i] != y[i])
              {
                       return false;
              }
          }
          return true;
     } 
     else
     {
         return false;
         }
}

int main(int argc, char *argv[])
{
    char ans;
do
{
    string a, b;
    cout << "String 1: \n";
    cin >> a;
    cout << "String 2: \n";
    cin >> b;
    
    if (compare (a,b))
    {
      cout << "String 1 and String 2 are the same." << endl;
      }
    else
    {
        cout << "String 1 and String 2 are not the same." << endl;
        } 
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
