/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 19, 2011, 10:04 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

// Define num function
void num()
{
    int x = 0;
    cout << x << endl;
    x++;
}

// X only exists inside staticNum
void staticNum()
{
    static int x;
    cout << "Static Variable" << x << endl;
    x++;
}

void swapNonReferenced(int x1, int x2)
{
    int temp = x1;
    x1 = x2;
    x2 = temp;
}

void outputVar(int x1, int x2)
{
    cout << "X: " << x1 << endl;
    cout << "Y: " << x2 << endl;
    cout << endl;
}

void swapRef(int & x1, int & x2)
{
    int temp = x1;
    x1 = x2;
    x2 = temp;
}
/*
 * 
 */
int main(int argc, char** argv) {
    
    /*for(int i = 0; i< 5; i++)
    {
        // Invoke Num
        num();
        staticNum();
    }*/
    int x = 3;
    int y = 4;
    
    //outputVar(x,y);
    //swapNonReferenced(x, y);
    //outputVar(x,y);
    
    //outputVar(x,y);
    //swapRef(x, y);
    //outputVar(x,y);
    
    //outputVar(5,y);
    //swapNonReferenced(5, y);
    //outputVar(5,y);
    
    outputVar(5,y);
    swapRef(5, y);
    outputVar(5,y);
    
    return 0;
}
