/*
* Name: James Grace
* Student ID: 2162001
* Date: 15 October 2013
* Lab: 7
* Problem: 5
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int fact(int num)
{
    int store;
    store = num;
    for(int i = 1; i < num; i++)
    {
            store = store * i;
            }
    return store;
}


int main(int argc, char *argv[])
{
    char ans;
do
{
    int x;
    cout << "Enter a number to find the factorial of that number: ";
    cin >> x;
    cout << endl << "The factorial of " << x << " is " << fact(x) << endl;
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
