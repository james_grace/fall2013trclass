/*
* Name: James Grace
* Student ID: 2162001
* Date: 15 October 2013
* Lab: 7
* Problem: 3
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

double power(double base, int expo)
{
       double final = pow(base, expo);
       return final;
}

int main(int argc, char *argv[])
{
    char ans, ans2;
do
{
    double x;
    int y;
    
    cout << "This program computes exponents of two given numbers.\n";
    cout << "Base: ";
    cin >> x;
    cout << endl << "Do you wish to enter an exponent? Enter Y for yes, all "
         "others will" << endl << "be the exponent: ";
    cin >> ans2;
    
    if (ans2 == 'y' || ans2 == 'Y')
    {
       cout << endl << "Exponent: ";
       cin >> y;
       cout << x << " to the power of " << y << " is " << power(x,y) << endl;
       }
    else
    {
        cout << endl << x << " to the power of 1 is " << power(x,1) << endl;
        }
    
    cout << endl << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
