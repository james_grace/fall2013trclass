/*
* Name: James Grace
* Student ID: 2162001
* Date: 18 September 2013
* HW: 3
* Problem: 2
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int apples, oranges, pears, atotal, ototal, ptotal;
        char ans;
do
{
    cout << "Please enter the amount of apples, oranges, and ";
    cout << "pears you wish to buy.\n";
    cout << "Apples: ";
    cin >> apples;
    cout << "Oranges: ";
    cin >> oranges;
    cout << "Pears: ";
    cin >> pears;
    cout << "\n";
    
    if (apples < oranges && apples < pears)
    {
              ototal = oranges - apples;
              ptotal = pears - apples;
              atotal = apples - apples;
              cout << "You must put back:\n";
              cout << atotal << " apples\n";
              cout << ototal << " oranges\n";
              cout << ptotal << " pears\n";
    }
    else if (oranges < apples && oranges < pears)
    {
         ototal = oranges - oranges;
         ptotal = pears - oranges;
         atotal = apples - oranges;
         cout << "You must put back:\n";
         cout << atotal << " apples\n";
         cout << ototal << " oranges\n";
         cout << ptotal << " pears\n";
         }
    else
    {
        ototal = oranges - pears;
        ptotal = pears - pears;
        atotal = apples - pears;
        cout << "You must put back:\n";
        cout << atotal << " apples\n";
        cout << ototal << " oranges\n";
        cout << ptotal << " pears\n";
        }
        
        cout << "Would you like to try again? (Y/N) ";
        cin >> ans;
        cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
