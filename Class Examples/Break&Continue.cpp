/*
* Name: James Grace
* Student ID: 2162001
* Date:  2013
* Lab: 
* Problem: 
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
do
{
    for(int i = 0; i < 10; i++)
    {
  
        if (i == 5)
            continue;
        
        cout << i << endl;
    }
    cout << endl;
    
    for(int i =0; i < 10; i++)
    {
  
        if (i == 5)
            break;
        
        cout << i << endl;
    }
    
    cout << endl;
    
    // For HW4 P.1
    cout << "Enter a number" << endl;
    
    int number;
    cin >> number;
    
    int num1000 = number / 1000;
    
    switch(num1000)
    {
        /*Adding break statements to the end will prevent the extra Ms, otherwise
          it will keep executing the other cases.
        */
        case 3: cout << "M";
        case 2: cout << "M"; 
        case 1: cout << "M"; 
    }
    
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";
    
    
    return EXIT_SUCCESS;
}
