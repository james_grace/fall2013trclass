/*
* Name: James Grace
* Student ID: 2162001
* Date: October 7 2013
* HW: 5
* Problem: 5
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

const double gallonsinaliter = 0.264179;
double conver (double num1, double num2)
       {
       return (num2 / (num1 * gallonsinaliter));
       }


int main(int argc, char *argv[])
{
    char ans;
do
{

    double mpg, miles, liters;
    
    cout << "Please enter liters used, and then miles driven for the car: \n";
    cin >> liters >> miles;
    
    
    cout << "Car Facts: \n";
    cout << "        Liters used: " << liters << endl;
    cout << "        Miles driven: " << miles << endl;
    cout << "        Miles per Gallon: " << conver(liters, miles) << endl;
    cout << "\n";


    cout << "Would you like to compute again? (Y/N) ";
    cin >> ans;
    cout << "\n";

} while (ans == 'y' || ans == 'Y');
    cout << "Goodbye.\n";


    return EXIT_SUCCESS;
}
