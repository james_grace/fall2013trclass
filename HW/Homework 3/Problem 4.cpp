/*
* Name: James Grace
* Student ID: 2162001
* Date: 18 Spetember 2013
* HW: 3
* Problem: 4
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char ans;
    int amtpeo, limit, excess, accept;
do
{
       cout << "Enter the maximum room occupancy: ";
       cin >> limit;
       cout << "Enter total number of people in attendance: ";
       cin >> amtpeo;

       
       excess = amtpeo - limit;
       accept = limit - amtpeo;

       
       if (amtpeo > limit)
        {
           cout << "This meeting is in "
                "violation of fire safety regulations.\n\a";
           cout << excess << 
           " people must leave the room to come within safety standards.\n";
        }
       else
        {
          cout << "This meeting is within fire safety standards.\n";
          cout << accept << " people may join the meeting.\n";
          }
    
    
    
    cout << "Would you like to try again? (Y/N) ";
    cin >> ans;
    cout << "\n";
    
} while (ans == 'y' || ans == 'Y');
    
    
    return EXIT_SUCCESS;
}
