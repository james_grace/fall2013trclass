/*
* Name: James Grace
* Student ID: 2162001
* Date: 17 September 2013
* Lab: 4.1
* Problem: 1
* I certify this is my own work and code
*/


#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    char grade, addit;
    

    cout << "This program calculates your grade on a 4.0 scale.\n";
    cout << "Please enter a letter grade: ";
    cin.get(grade);
    cin.get(addit);
    
    switch (grade)
    {
           case 'A':
                switch (addit)
                {
                       case '+':
                            cout << "This is a 4.0\n";
                            break;
                       case '-':
                            cout << "This is a 3.7\n";
                            break;
                       default:
                            cout << "This is a 4.0\n";
                }
                break;
           case 'B':
                switch (addit)
                {
                       case '+':
                            cout << "This is a 3.3\n";
                            break;
                       case '-':
                            cout << "This is a 2.7\n";
                            break;
                       default:
                            cout << "This is a 3.0\n";
                }
                break;     
    default:
            cout << "Please try again. Thank you.\n";
}    
    system("PAUSE"); 
    return EXIT_SUCCESS;
}
